# coding=utf-8
from django.contrib.sites.managers import CurrentSiteManager
from django.contrib.sites.models import Site
from django.db import models
from django.utils.translation import ugettext_lazy as _
from fields import JSONField


class TaxonomyNodeManager(models.Manager):
    def get_by_natural_key(self, name):
        return self.get(name=name)


class TaxonomyNode(models.Model):
    name = models.CharField(max_length=100, unique=True, verbose_name=_(u"name"))
    description = models.CharField(max_length=200, verbose_name=_(u"description"), null=True, blank=True)
    data = JSONField(_(u"extra data"), blank=True)

    class Meta(object):
        app_label = 'taxonomy'
        abstract = True

    def __unicode__(self):
        return u"%s" % self.name

    def natural_key(self):
        return self.name,

    def extra_attribute(self, attribute, default=None):
        return self.data.get(attribute, default)


class Classifier(TaxonomyNode):
    targets = models.ManyToManyField(
        'Descriptor',
        related_name='classifiers',
        blank=True,
        verbose_name=_(u"targets")
    )
    code = models.CharField(max_length=10, verbose_name=_(u"code"))

    objects = TaxonomyNodeManager()

    class Meta(object):
        app_label = 'taxonomy'
        verbose_name = _(u"classifier")
        verbose_name_plural = _(u"classifiers")


class CurrentSiteTaxonomyNodeManager(TaxonomyNodeManager, CurrentSiteManager):
    pass


class Descriptor(TaxonomyNode):
    target = models.ForeignKey(Classifier, related_name='values', verbose_name=_(u"target"))
    rank = models.PositiveSmallIntegerField(default=1, verbose_name=_(u"rank"))
    sites = models.ManyToManyField(Site, verbose_name=_(u"sites"))

    objects = TaxonomyNodeManager()
    on_site = CurrentSiteTaxonomyNodeManager()

    class Meta(object):
        app_label = 'taxonomy'
        verbose_name = _(u"descriptor")
        verbose_name_plural = _(u"descriptors")
        ordering = ('rank', 'name')


class DescriptorsRelationshipCategory(models.Model):
    name = models.CharField(max_length=50, verbose_name=_(u"name"))
    description = models.TextField(verbose_name=_(u'description'), null=True, blank=True)

    class Meta(object):
        app_label = 'taxonomy'
        verbose_name = _(u"descriptor relationship category")
        verbose_name_plural = _(u"descriptor relationship categories")

    def __unicode__(self):
        return u"%s" % self.name


class DescriptorsRelationship(models.Model):
    main = models.ForeignKey(Descriptor, verbose_name=_(u'main descriptor'),
                             related_name="outgoing_relationships")
    secondary = models.ForeignKey(Descriptor, verbose_name=_(u'secondary descriptor'),
                                  related_name="incoming_relationships")
    category = models.ForeignKey(DescriptorsRelationshipCategory, verbose_name=_(u'category'))

    class Meta(object):
        app_label = 'taxonomy'
        verbose_name = _(u"descriptor relationship")
        verbose_name_plural = _(u"descriptor relationships")
        unique_together = ('main', 'secondary', 'category')
