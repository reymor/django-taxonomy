# coding=utf-8
from django.test import TestCase
from django.conf import settings
from ..models import Classifier


class TestTaxonomy(TestCase):
    fixtures = ['test_data.json']

    def setUp(self):
        pass

    def test_basics(self):
        currency_classifier = Classifier.objects.get(pk=1)
        self.assertEqual(currency_classifier.code, 'CUR')

        self.assertEqual(currency_classifier.values.count(), 3)

    def test_site_awareness(self):
        currency_classifier = Classifier.objects.get(pk=1)
        self.assertEqual(currency_classifier.values.count(), 3)
        setattr(settings, 'SITE_ID', 2)
        self.assertEqual(currency_classifier.values.count(), 2)
