# coding=utf-8
from django.contrib.admin import site, ModelAdmin, TabularInline
from .models import Classifier, Descriptor


class DescriptorAdmin(ModelAdmin):
    list_display = ('name', 'target', 'description')
    list_filter = ('target',)


class DescriptorInline(TabularInline):
    model = Descriptor
    fk_name = 'target'
    fields = ('name', 'description')


class ClassifierAdmin(ModelAdmin):
    list_display = ('code', 'name', 'description')
    inlines = [DescriptorInline]


site.register(Descriptor, DescriptorAdmin)
site.register(Classifier, ClassifierAdmin)
