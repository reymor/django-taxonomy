# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Descriptor.data'
        db.add_column(u'taxonomy_descriptor', 'data',
                      self.gf('taxonomy.fields.JSONField')(default='', blank=True),
                      keep_default=False)


        # Changing field 'Descriptor.rank'
        db.alter_column(u'taxonomy_descriptor', 'rank', self.gf('django.db.models.fields.PositiveSmallIntegerField')())
        # Adding field 'Classifier.data'
        db.add_column(u'taxonomy_classifier', 'data',
                      self.gf('taxonomy.fields.JSONField')(default='', blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Descriptor.data'
        db.delete_column(u'taxonomy_descriptor', 'data')


        # Changing field 'Descriptor.rank'
        db.alter_column(u'taxonomy_descriptor', 'rank', self.gf('django.db.models.fields.PositiveSmallIntegerField')(max_length=2))
        # Deleting field 'Classifier.data'
        db.delete_column(u'taxonomy_classifier', 'data')


    models = {
        u'sites.site': {
            'Meta': {'ordering': "(u'domain',)", 'object_name': 'Site', 'db_table': "u'django_site'"},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'taxonomy.classifier': {
            'Meta': {'object_name': 'Classifier'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'data': ('taxonomy.fields.JSONField', [], {'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'targets': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'classifiers'", 'blank': 'True', 'to': "orm['taxonomy.Descriptor']"})
        },
        'taxonomy.descriptor': {
            'Meta': {'ordering': "('rank', 'name')", 'object_name': 'Descriptor'},
            'data': ('taxonomy.fields.JSONField', [], {'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'rank': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'sites': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['sites.Site']", 'symmetrical': 'False'}),
            'target': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'values'", 'to': "orm['taxonomy.Classifier']"})
        },
        'taxonomy.descriptorsrelationship': {
            'Meta': {'unique_together': "(('main', 'secondary', 'category'),)", 'object_name': 'DescriptorsRelationship'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['taxonomy.DescriptorsRelationshipCategory']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'outgoing_relationships'", 'to': "orm['taxonomy.Descriptor']"}),
            'secondary': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'incoming_relationships'", 'to': "orm['taxonomy.Descriptor']"})
        },
        'taxonomy.descriptorsrelationshipcategory': {
            'Meta': {'object_name': 'DescriptorsRelationshipCategory'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['taxonomy']