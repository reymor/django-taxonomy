# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Classifier'
        db.create_table(u'taxonomy_classifier', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('code', self.gf('django.db.models.fields.CharField')(max_length=10)),
        ))
        db.send_create_signal(u'taxonomy', ['Classifier'])

        # Adding M2M table for field targets on 'Classifier'
        m2m_table_name = db.shorten_name(u'taxonomy_classifier_targets')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('classifier', models.ForeignKey(orm[u'taxonomy.classifier'], null=False)),
            ('descriptor', models.ForeignKey(orm[u'taxonomy.descriptor'], null=False))
        ))
        db.create_unique(m2m_table_name, ['classifier_id', 'descriptor_id'])

        # Adding model 'Descriptor'
        db.create_table(u'taxonomy_descriptor', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('target', self.gf('django.db.models.fields.related.ForeignKey')(related_name='values', to=orm['taxonomy.Classifier'])),
        ))
        db.send_create_signal(u'taxonomy', ['Descriptor'])

        # Adding M2M table for field sites on 'Descriptor'
        m2m_table_name = db.shorten_name(u'taxonomy_descriptor_sites')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('descriptor', models.ForeignKey(orm[u'taxonomy.descriptor'], null=False)),
            ('site', models.ForeignKey(orm[u'sites.site'], null=False))
        ))
        db.create_unique(m2m_table_name, ['descriptor_id', 'site_id'])

        # Adding model 'DescriptorsRelationshipCategory'
        db.create_table(u'taxonomy_descriptorsrelationshipcategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'taxonomy', ['DescriptorsRelationshipCategory'])

        # Adding model 'DescriptorsRelationship'
        db.create_table(u'taxonomy_descriptorsrelationship', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('main', self.gf('django.db.models.fields.related.ForeignKey')(related_name='outgoing_relationships', to=orm['taxonomy.Descriptor'])),
            ('secondary', self.gf('django.db.models.fields.related.ForeignKey')(related_name='incoming_relationships', to=orm['taxonomy.Descriptor'])),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['taxonomy.DescriptorsRelationshipCategory'])),
        ))
        db.send_create_signal(u'taxonomy', ['DescriptorsRelationship'])

        # Adding unique constraint on 'DescriptorsRelationship', fields ['main', 'secondary', 'category']
        db.create_unique(u'taxonomy_descriptorsrelationship', ['main_id', 'secondary_id', 'category_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'DescriptorsRelationship', fields ['main', 'secondary', 'category']
        db.delete_unique(u'taxonomy_descriptorsrelationship', ['main_id', 'secondary_id', 'category_id'])

        # Deleting model 'Classifier'
        db.delete_table(u'taxonomy_classifier')

        # Removing M2M table for field targets on 'Classifier'
        db.delete_table(db.shorten_name(u'taxonomy_classifier_targets'))

        # Deleting model 'Descriptor'
        db.delete_table(u'taxonomy_descriptor')

        # Removing M2M table for field sites on 'Descriptor'
        db.delete_table(db.shorten_name(u'taxonomy_descriptor_sites'))

        # Deleting model 'DescriptorsRelationshipCategory'
        db.delete_table(u'taxonomy_descriptorsrelationshipcategory')

        # Deleting model 'DescriptorsRelationship'
        db.delete_table(u'taxonomy_descriptorsrelationship')


    models = {
        u'sites.site': {
            'Meta': {'ordering': "(u'domain',)", 'object_name': 'Site', 'db_table': "u'django_site'"},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'taxonomy.classifier': {
            'Meta': {'object_name': 'Classifier'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'targets': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'classifiers'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['taxonomy.Descriptor']"})
        },
        u'taxonomy.descriptor': {
            'Meta': {'object_name': 'Descriptor'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'sites': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['sites.Site']", 'symmetrical': 'False'}),
            'target': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'values'", 'to': u"orm['taxonomy.Classifier']"})
        },
        u'taxonomy.descriptorsrelationship': {
            'Meta': {'unique_together': "(('main', 'secondary', 'category'),)", 'object_name': 'DescriptorsRelationship'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['taxonomy.DescriptorsRelationshipCategory']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'outgoing_relationships'", 'to': u"orm['taxonomy.Descriptor']"}),
            'secondary': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'incoming_relationships'", 'to': u"orm['taxonomy.Descriptor']"})
        },
        u'taxonomy.descriptorsrelationshipcategory': {
            'Meta': {'object_name': 'DescriptorsRelationshipCategory'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['taxonomy']