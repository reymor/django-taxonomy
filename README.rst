==========================
Django Taxonomy
==========================

Taxonomy for Django>=1.6.1

Allows to configure the taxonomy your project will use.
One Classifier has several possible values, called Descriptors.
Classifiers can be configure to target one or several specific descriptors (targeting other classifiers themselves),
becoming 'not root' classifiers.
Classifiers that don't specifically target any descriptor are 'root' classifiers.

Changelog
=========

0.2.5
-----

Restored default manager (returning all items).

0.2.4
-----

Fix in migrations.

0.2.3
-----

Added a new JSON field for storing extra attributes in each taxonomy node.

0.2.2
-----

Fixes for compatibility with Django 1.8 (avoid system check warnings).

0.2.1
-----

Completed Spanish translations.

0.2.0
-----

Adding the possibility of ranking descriptors, default descriptors ordering is now by rank, then by name.

0.1.0
-----

PENDING...

Notes
-----

PENDING...

Usage
-----

1. Run ``python setup.py install`` to install.

2. Modify your Django settings to use ``taxonomy``: